#!/usr/bin/env ruby

require "oauth2"
require "business"
require "yaml"

def get_uptime(token, username, min_date, max_date)
	uptime = Hash.new(0)

	min = min_date.to_time.utc.strftime("%Y-%m-%dT%H:%M:%S.%LZ")
	max = max_date.to_time.utc.strftime("%Y-%m-%dT%H:%M:%S.%LZ")

	location_url = "/v2/users/#{username}/locations?range[begin_at]=#{min},#{max}"

	page = 1
	page_size = 100

	loop do
		location_data = token.get(location_url,
			params: {page: {number: page, size: page_size}})

		location_data.parsed.each {
			|location|

			raise StandardError unless location["begin_at"]
			
			begin_at = DateTime.parse(location["begin_at"])
			end_at = location["end_at"] ? DateTime.parse(location["end_at"]) : DateTime.now
			
			uptime[begin_at.to_date] += (end_at.to_time - begin_at.to_time)
		}
		
		break if (location_data.parsed.length != page_size)
		
		page++
		sleep(1)
	end

	return uptime
end

# Load config
url, uid, secret = YAML.load_file("#{__dir__}/config.yml")

# Get username
username = ARGV.length == 1 ? ARGV[0] : ENV["USER"]
#printf("User:    %s\n", user)

# Create the client with your credentials
client = OAuth2::Client.new(uid, secret, site: url)

# Get an access token
token = client.client_credentials.get_token

# Get location data and uptime
today = Date.today
min_date = today - today.mday + 1
max_date = today + 1
uptime = get_uptime(token, username, min_date, max_date)

# Get the working days
Business::Calendar.load_paths = [
	"lib/calendars",
]

calendar = Business::Calendar.load("targetfrance")

# Print every active day
total = 0
count = 0

for day in 1..today.day do
	date = Date.new(today.year, today.month, day)
	count += 1 if calendar.business_day?(date)
	total += uptime[date]
	h = uptime[date] / 3600
	m = (h % 1) * 60
end

# Print average
if total > 0 then
	h = (total / 3600)
	m = (h % 1) * 60
end

month_total = calendar.business_days_between(Date.new(today.year, today.month, 1), Date.new(today.year, today.month, -1)) * 7
due_total = calendar.business_days_between(Date.new(today.year, today.month, 1), today) * 7

printf("Total uptime:   %02d:%02d/%02d\n", h, m, month_total);
printf("Average uptime: %02d:%02d\n", (h / count).floor, (m / count).floor) if count > 0
