#!/usr/bin/env ruby

require 'yaml'
require 'oauth2'
require 'business'
require 'icalendar'

def get_uptime(token, username, min_date, max_date)
	uptime = 0

	min = min_date.to_time.utc.strftime("%Y-%m-%dT%H:%M:%S.%LZ")
	max = max_date.to_time.utc.strftime("%Y-%m-%dT%H:%M:%S.%LZ")

	location_url = "/v2/users/#{username}/locations?range[begin_at]=#{min},#{max}"

	page = 1
	page_size = 100

	loop do
		location_data = token.get(location_url,
			params: {page: {number: page, size: page_size}})

		location_data.parsed.each {
			|location|

			raise StandardError unless location["begin_at"]

			begin_at = DateTime.parse(location["begin_at"])
			end_at = location["end_at"] ? DateTime.parse(location["end_at"]) : DateTime.now

			uptime += (end_at.to_time - begin_at.to_time)
		}

		break if (location_data.parsed.length != page_size)

		page++
		sleep(1)
	end

	return uptime
end

# Load config
url, uid, secret = YAML.load_file("#{__dir__}/config.yml")

# Get apprenticeship id
apprenticeship_id = ARGV.length >= 1 ? ARGV[0] : "2023-october_2y"

# Get username
username = ARGV.length >= 2 ? ARGV[1] : ENV["USER"]

# Create the client with your credentials
client = OAuth2::Client.new(uid, secret, site: url)

# Get an access token
token = client.client_credentials.get_token

# Get the working days
Business::Calendar.load_paths = ["lib/calendars"]

business_cal = Business::Calendar.load("targetfrance")

# Load apprenticeship calendar
apprenticeship_cal_file = File.open("#{__dir__}/lib/calendars/apprenticeships/#{apprenticeship_id}.ics")

apprenticeship_cals = Icalendar::Calendar.parse(apprenticeship_cal_file)
apprenticeship_cal = apprenticeship_cals.first

#export_cal = Icalendar::Calendar.new

today = Date.today

apprenticeship_periods = apprenticeship_cal.events.select {
	|event| event.summary == "42" && event.dtstart <= today
}

apprenticeship_periods.sort_by! {
	|event_a| event_a.dtstart
}

for event in apprenticeship_periods do
#	export_cal.add_event(event)

	puts "#{event.dtstart} - #{event.dtend}"

	uptime = get_uptime(token, username, event.dtstart, event.dtend)

	uptime_h = uptime / 3600
	uptime_m = uptime_h % 1 * 60

	due_h = business_cal.business_days_between(event.dtstart, event.dtend) * 7
	due_m = 0

	overtime_h = uptime_h - due_h
	overtime_m = overtime_h % 1 * 60

	printf("- uptime: %02d:%02d\n", uptime_h.floor, uptime_m.floor)
	printf("- due: %02d:%02d\n", due_h.ceil, due_m.ceil)
	printf("- overtime: %02d:%02d\n", overtime_h.floor, overtime_m.floor)

	puts

	# Cooldown
	sleep(0.51)
end

#puts export_cal.to_ical